# e621-Client and Command-Line-Interface
Download some yiffy media from [e621](https://e621.net)!


### Download Media
Setup config.json:

```json
{
	"agent": "e621-api",
	"limit": 400,
	"folder": "./media",
	"format": "{ID}.{EXT}",
	"useAuthorName": false,
	"overwrite": false,
	"useIndex": true,
	"showInfo": false,
	"key": "",
	"username": "",
	"page": 0
}
```

```bash
$node main.mjs <tags separated by space>
```