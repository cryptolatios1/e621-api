import https from "https";

/**@param {string[]} tags @param {string} agent @param {number} limit @param {number} page @return {Promise<{posts: Array<{id: number, created_at: String, updated_at: String, file: {width: Number, height: Number, ext: String, size: Number, md5: String, url: String}, preview: {width: Number, height: Number, url: String}, sample: {has: Boolean, height: Number, width: Number, url: String, alternates: {}}, score: {up: Number, down: Number, total: Number}, tags: {general: Array<String>, species: Array<String>, character: Array<String>, copyright: Array<String>, artist: Array<String>, invalid: Array<String>, lore: Array<String>, meta: Array<String>}, locked_tags: Array<String>, change_seq: Number, flags: {pending: Boolean, flagged: Boolean, note_locked: Boolean, status_locked: Boolean, rating_locked: Boolean, deleted: Boolean}, rating: "e" | "q" | "s", fav_count: Number, sources: Array<String>, pools: Array<Number>, relationships: {parend_id: Number, has_children: Boolean, has_active_children: Boolean, children: Array<Number>}, approver_id: Number, uploader_id: Number, description: String, comment_count: Number, is_favorited: Boolean, has_notes: Boolean, duration: Number}>}>}*/
export let fetchPosts = (tags, agent, limit = 300, page = 1, key = undefined, username = undefined) => new Promise((resolve, reject) => {
	https.get(`https://e621.net/posts.json?limit=${limit}&page=${page}${key && key.length && username && username.length ? `&api_key=${key}&login=${username}` : ""}${tags && tags.length ? `&tags=${tags.join("+").replace(" ", "_")}` : ""}`, {headers: {"user-agent": agent}}, async msg => {
		msg.setEncoding("utf8");
		let content = "";
		for await (let body of msg) content += body;
		try {
			let json = JSON.parse(content);
			resolve(json);
		} catch (error) {
			reject(error);
		};
	}).on("error", reject);
});

/**@param {string|URL} url @return {Buffer}*/
export let fetchImageBuffer = (url, options = {}) => new Promise((resolve, reject) => {
	https.get(url, options, async msg => {
		let data = [];
		for await(let body of msg) data.push(body);
		resolve(Buffer.concat(data));
	}).on("error", reject);
});