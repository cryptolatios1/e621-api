import fs from "fs";
import {fetchPosts, fetchImageBuffer} from "./core/api.mjs";

/* === Parameter === */
let args = process.argv.slice(2);
let useAuthorNameParam = args.indexOf("--use-author");
if(useAuthorNameParam !== -1) {
	args.splice(useAuthorNameParam, 1);
	useAuthorNameParam = true;
} else useAuthorNameParam = undefined;

let overwriteParam = args.indexOf("--overwrite");
if(overwriteParam !== -1) {
	args.splice(overwriteParam, 1);
	overwriteParam = true;
} else overwriteParam = undefined;

let useIndexParam = args.indexOf("--use-index");
if(useIndexParam !== -1) {
	args.splice(useIndexParam, 1);
	useIndexParam = true;
} else useIndexParam = undefined;

let agentParam = args.indexOf("--agent");
if(agentParam !== -1) {
	agentParam = args.splice(agentParam, 2)[1];
	if(!agentParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --agent <user-agent>\n`);
		process.exit();
	};
} else agentParam = undefined;

let limitParam= args.indexOf("--limit");
if(limitParam !== -1) {
	limitParam = args.splice(limitParam, 2)[1];
	if(!limitParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --limit <limit>\n`);
		process.exit();
	};
} else limitParam = undefined;

let folderParam = args.indexOf("--folder");
if(folderParam !== -1) {
	folderParam = args.splice(folderParam, 2)[1];
	if(!folderParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --folder <folder>\n`);
		process.exit();
	};
} else folderParam = undefined;

let formatParam = args.indexOf("--format");
if(formatParam !== -1) {
	formatParam = args.splice(formatParam, 2)[1];
	if(!formatParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --format <format>\n`);
		process.exit();
	};
} else formatParam = undefined;

let keyParam = args.indexOf("--key");
if(keyParam !== -1) {
	keyParam = args.splice(keyParam, 2)[1];
	if(!keyParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --key <api-key>\n`);
		process.exit();
	};
} else keyParam = undefined;

let usernameParam = args.indexOf("--username");
if(usernameParam !== -1) {
	usernameParam = args.splice(usernameParam, 2)[1];
	if(!usernameParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --username <api-key>\n`);
		process.exit();
	};
} else usernameParam = undefined;

let pageParam = args.indexOf("--page");
if(pageParam !== -1) {
	pageParam = args.splice(pageParam, 2)[1];
	if(!pageParam) {
		process.stdout.write(`\x1b[31m[Error]\x1b[0m Usage: --page <start-page>\n`);
		process.exit();
	};
} else pageParam = undefined;

let infoParam = args.indexOf("--info");
if(infoParam !== -1) {
	infoParam = true;
} else infoParam = undefined;

let config = fs.existsSync("./config.json") ? JSON.parse(fs.readFileSync("./config.json", "utf8")) : {};

/* Overwrite config (if given) */
let agent = String(agentParam ?? config.agent ?? "e621-api");
let limit = Number(limitParam ?? config.limit ?? 300);
let folder = String(folderParam ?? config.folder ?? "./images");
let format = String(formatParam ?? config.format ?? "{ID}.{EXT}");
let useAuthorName = Boolean(useAuthorNameParam ?? config.useAuthorName ?? false);
let overwrite = Boolean(overwriteParam ?? config.overwrite ?? false);
let useIndex = Boolean(useIndexParam ?? config.useIndex ?? false);
let showInfo = Boolean(infoParam ?? config.showInfo ?? false);
let key = String(keyParam ?? config.key ?? "");
let username = String(usernameParam ?? config.username ?? "");
let page = String(pageParam ?? config.page ?? 0); /* string because pages can also be bXYXYXYXY (before) or aXYXYXYXY (after)*/
let lastId;

/**@type {string[]}*/
let tags = args;
let mainLimit = limit;
let mainPage = /^[0-9]{1,3}$/.test(page) ? Number(page) : 751;
let countedPosts = 0;
let index = useIndex && fs.existsSync("./index.json") ? JSON.parse(fs.readFileSync("./index.json", "utf8")) : {};

if(limit === Infinity) process.stdout.write("\x1b[31m[Warning]\x1b[0m Infinity limit downloads every post with given tags\n");
if(limit > 300) limit = 300;

/* both of them are required or none */
if((username.length !== 0 || key.length !== 0) && (username.length === 0 || key.length === 0)) {
	process.stdout.write("\x1b[31m[Warning]\x1b[0m missing username or key, disabling login\n");
	username = undefined;
	key = undefined;
};

if(tags.length > 40) {
	process.stdout.write("\x1b[31m[Warning]\x1b[0m over 40 tags, reducing to 40\n");
	tags = tags.slice(0, 39);
};

/* main loop */
/* 4 possibilitys: No posts, less posts than wanted, exactly as much posts, more posts */
while(countedPosts < mainLimit) {

	/* fetch posts */
	let data = (await fetchPosts(tags, agent, limit, mainPage <= 750 ? mainPage : (lastId !== undefined ? `b${lastId}` : page), key, username).catch(console.error));
	let posts = data?.posts;
	if(typeof posts !== "object") {
		process.stdout.write(`[Error] ${data?.message ?? "fetch-error"}`);
		break;
	};

	for(let postIndex = 0; postIndex < posts.length; postIndex++) {
		let post = posts[postIndex];
		if(!post.file.url) {
			process.stdout.write(`[Blocked] ${post.id}\n`);
			continue;
		};

		lastId = post.id;

		/* get file name and folder */
		let name = format.replace("{ID}", post.id).replace("{EXT}", post.file.ext).replace("{MD5}", post.file.md5);
		let destination = folderParam || !useAuthorName ? folder : `${folder}/${post.tags.artist[0]}/`;
	
		/* check if exists and if the file should be overwriten */
		if(!fs.existsSync(destination)) fs.mkdirSync(destination, {recursive: true});
		else if(fs.existsSync(`${destination}/${name}`) && !overwrite) {
			process.stdout.write(`[Exists] ${post.id}\n`);
			continue;
		};

		if(showInfo) console.log(post);

		/* now download file and write to destination/name */
		let buffer = await fetchImageBuffer(post.file.url, {headers: {"user-agent": agent}}).catch(console.log);
		if(buffer) fs.writeFileSync(`${destination}/${name}`, buffer);

		if(useIndex) index[post.id] = post;

		process.stdout.write(`[Finish] ${post.id} ${++countedPosts}/${mainLimit}${!buffer ? ` Error` : ``}\n`);
	};

	process.stdout.write(`[Finished] Page ${page}\n`);
	if(posts.length < limit) break;

	mainPage++;
};

if(useIndex) fs.writeFileSync("./index.json", JSON.stringify(index, undefined, "\t"));
